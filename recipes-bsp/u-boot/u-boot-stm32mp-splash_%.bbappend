FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

UBOOT_SPLASH_LANDSCAPE_SRC = "splash_tns_landscape.bmp"
UBOOT_SPLASH_PORTRAIT_SRC = "splash_tns_portrait.bmp"
UBOOT_SPLASH_SRC = "splash_tns_portrait.bmp"
