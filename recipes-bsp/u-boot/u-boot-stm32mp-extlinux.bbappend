FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI += "file://rproc-m4-fw.elf"

do_install:append () {
    install -m 755 ${WORKDIR}/rproc-m4-fw.elf ${D}/${UBOOT_EXTLINUX_INSTALL_DIR}
}