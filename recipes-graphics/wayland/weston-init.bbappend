FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI += "file://weston.ini \ 
            file://icons/\
            file://tns-background.jpg"

FILES:${PN} += " /usr/share"

do_install_append () {
    install -d ${D}/usr/share/backgrounds
    install -m 644 ${WORKDIR}/tns-background.jpg ${D}/usr/share/backgrounds/

    install -d ${D}/usr/share/icons/24x24/apps
    for file in ${WORKDIR}/icons/*;do
        install -m 755 "$file" ${D}/usr/share/icons/24x24/apps/
    done
}