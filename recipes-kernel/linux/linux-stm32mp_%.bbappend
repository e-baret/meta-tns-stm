FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI += "file://${LINUX_VERSION}/"
SRC_URI += "file://${LINUX_VERSION}/0024-ARM-5.10-stm32mp1-devicetree-coproc.patch"
#SRC_URI += "file://${LINUX_VERSION}/0025-ARM-5.10-stm32mp1-DMA.patch"
SRC_URI += "file://${LINUX_VERSION}/0026-ARM-5.10-stm32mp1-devicetree-CAN.patch"

#config fragmentfiles
SRC_URI += "file://cfg/bluetooth.cfg"
SRC_URI += "file://cfg/CAN.cfg"
SRC_URI += "file://cfg/ftrace.cfg"
SRC_URI += "file://cfg/preempt-rt.cfg"
SRC_URI += "file://cfg/rpmsg.cfg"
SRC_URI += "file://cfg/sched_debug.cfg"

#feature files
SRC_URI += "file://cfg/bluetooth.scc"
SRC_URI += "file://cfg/CAN.scc"
SRC_URI += "file://cfg/ftrace.scc"
SRC_URI += "file://cfg/preempt-rt.scc"
SRC_URI += "file://cfg/rpmsg.scc"
SRC_URI += "file://cfg/sched_debug.scc"

KERNEL_FEATURES_append += " bluetooth.scc"
KERNEL_FEATURES_append += " CAN.scc"
KERNEL_FEATURES_append += " ftrace.scc"
KERNEL_FEATURES_append += " rpmsg.scc"
KERNEL_FEATURES_append += " preempt-rt.scc"
KERNEL_FEATURES_append += " sched_debug.scc"

KERNEL_CONFIG_FRAGMENTS += " ${WORKDIR}/cfg/bluetooth.cfg"
KERNEL_CONFIG_FRAGMENTS += " ${WORKDIR}/cfg/CAN.cfg"
KERNEL_CONFIG_FRAGMENTS += " ${WORKDIR}/cfg/ftrace.cfg"
KERNEL_CONFIG_FRAGMENTS += " ${WORKDIR}/cfg/preempt-rt.cfg"
KERNEL_CONFIG_FRAGMENTS += " ${WORKDIR}/cfg/rpmsg.cfg"
KERNEL_CONFIG_FRAGMENTS += " ${WORKDIR}/cfg/sched_debug.cfg"