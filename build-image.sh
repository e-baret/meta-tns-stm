#!/bin/sh 
SRCDIR=src-gitlab
WORKDIR=project-stm32mp1
IMGFILE=FlashLayout_sdcard_stm32mp157f-dk2-trusted

#set -x
if [ -z "$1" ]
then
    echo "Please give the name of the image to build"
    exit
fi

if [ $HOME == "/home/elf" ]
then
    DEPLOYDIR=$HOME/workspace/build/tmp/deploy/images/stm32mp1
else
    DEPLOYDIR=$HOME/$SRCDIR/$WORKDIR/build/tmp/deploy/images/stm32mp1
fi

IMGNAME=$1
FILE=$DEPLOYDIR/flashlayout_$IMGNAME
echo $FILE
if [ ! -d $FILE ]; then
    echo "Image doesn't exist or doesn't compile : $1"
    exit
fi  

if [ ! -e $DEPLOYDIR ]; then
    echo "Folder doesn't exist : $DEPLOYDIR"
    exit
fi

# remove old images
if [ -e $DEPLOYDIR/$IMGFILE.raw ]
then
    rm $DEPLOYDIR/$IMGFILE.raw
fi

# builds images 
$DEPLOYDIR/scripts/create_sdcard_from_flashlayout.sh $DEPLOYDIR/flashlayout_$IMGNAME/trusted/$IMGFILE.tsv

# gives informations of images
if [ -e $DEPLOYDIR/$IMGFILE.raw ]
then
    echo "Image is created in  $DEPLOYDIR"
    du -h $DEPLOYDIR/$IMGFILE.raw
fi
