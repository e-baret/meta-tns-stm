SUMMARY = ""
DESCRIPTION = ""

LICENSE= "CLOSED"

FILES:${PN} += " /home/root"

SRC_URI += "file://src/ \
            file://src/can-read \ 
            file://src/can-write \
            file://scripts/ \
            "
                     
INHIBIT_SYSROOT_STRIP = "1"

#avoid error message
INSANE_SKIP_${PN}_append = "already-stripped"

DESTDIR = "/home/root/dev/CAN-bus"

DEPENDS = " can-utils iproute2" 

do_install () {
    install -d ${D}${DESTDIR}
    install -d ${D}${DESTDIR}/can-read
    install -d ${D}${DESTDIR}/can-write
    for file in ${WORKDIR}/src/can-read/*;do
        install -m 755 "$file" ${D}${DESTDIR}/can-read
    done

    for file in ${WORKDIR}/src/can-write/*;do
        install -m 755 "$file" ${D}${DESTDIR}/can-write
    done
    
    for file in ${WORKDIR}/scripts/*;do
        install -m 755 "$file" ${D}${DESTDIR}
    done
}

PACKAGE_ARCH = "${MACHINE_ARCH}"