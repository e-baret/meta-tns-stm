#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <linux/can.h>
#include <sys/socket.h>
#include <net/if.h>
//#include <linux/if_packet.h>


int main(){

    // create empty socket with PF_CAN protocol
    int s = socket(PF_CAN, SOCK_RAW, CAN_RAW);

    //bind socket to interface
    struct sockaddr_can addr;
    struct ifreq ifr;

    strcpy(ifr.ifr_name, "can0");
    ioctl(s, SIOCGIFINDEX, &ifr);

    addr.can_family = AF_CAN;
    addr.can_ifindex = ifr.ifr_ifindex;

    bind(s, (struct sockaddr *)&addr, sizeof(addr));

    struct can_frame frame;
    // Read CAN frames
    while(1){
        int nbytes = read(s, &frame, sizeof(struct can_frame)); //Extract frame.id, frame.can_dlc, frame.data[i]
        switch(nbytes){
            case -1 : printf("error read\n"); exit(1); break;
            case 0 : printf("no data read\n"); break;
            default : printf("rcv[%d]: %x [%d]",nbytes, frame.can_id, frame.can_dlc);
                    for(int i=0; i<frame.can_dlc; i++){printf(" %x ", frame.data[i]);}
                    printf("\n");
                    break;
        }
    }
    // closing the socket 
    close(s);
}