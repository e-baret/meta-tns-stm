- Scripts : 

    - ./up.sh : config and wake up can0 and can1

    - ./down.sh : shut down can0 and can1

    - ./loopback-up.sh : config and wake up can0 and can1 with loopback enable

- Cmd : 

    - Show can stats : cat /proc/net/can/stats

    - read can0 : candump can0

    - write on can0 : cansend can0 123#1122334455667788

    - write on can0 : cangen can0 -v

    - if during "cangen canx" no buffer space is available, run : ifconfig can0 txqueuelen 1000


