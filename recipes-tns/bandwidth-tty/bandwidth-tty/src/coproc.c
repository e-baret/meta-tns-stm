#include "coproc.h"

int copro_isFwRunning(void)
{
    int fd;
    size_t byte_read;
    int result = 0;
    unsigned char bufRead[MAX_BUF];
    fd = open("/sys/class/remoteproc/remoteproc0/state", O_RDWR);
    if (fd < 0) {
        printf("CA7 : Error opening remoteproc0/state, err=-%d\n", errno);
        return (errno * -1);
    }
    byte_read = (size_t) read (fd, bufRead, MAX_BUF);
    if (byte_read >= strlen("running")) {
        char* pos = strstr((char*)bufRead, "running");
        if(pos) {
            result = 1;
        }
    }
    close(fd);
    return result;
}
 
int copro_stopFw(void)
{
    int fd;
    fd = open("/sys/class/remoteproc/remoteproc0/state", O_RDWR);
    if (fd < 0) {
        printf("CA7 : Error opening remoteproc0/state, err=-%d\n", errno);
        return (errno * -1);
    }
    write(fd, "stop", strlen("stop"));
    close(fd);
    return 0;
}
 
int copro_startFw(void)
{
    int fd;
    fd = open("/sys/class/remoteproc/remoteproc0/state", O_RDWR);
    if (fd < 0) {
        printf("CA7 : Error opening remoteproc0/state, err=-%d\n", errno);
        return (errno * -1);
    }
    write(fd, "start", strlen("start"));
    close(fd);
    return 0;
}
 
int copro_getFwName(char* pathStr)
{
    int fd;
    int byte_read;
    fd = open("/sys/class/remoteproc/remoteproc0/firmware", O_RDWR);
    if (fd < 0) {
        printf("CA7 : Error opening remoteproc0/firmware, err=-%d\n", errno);
        return (errno * -1);
    }
    byte_read = read (fd, pathStr, MAX_BUF);
    close(fd);
    return byte_read;
}
 
int copro_setFwName(char* nameStr)
{
    int fd;
    int result = 0;
    fd = open("/sys/class/remoteproc/remoteproc0/firmware", O_RDWR);
    if (fd < 0) {
        printf("CA7 : Error opening remoteproc0/firmware, err=-%d\n", errno);
        return (errno * -1);
    }
    result = write(fd, nameStr, strlen(nameStr));
    close(fd);
    return result;
}

void copro_Init(char* firmwareName){
   char buffer[MAX_BUF]={'\0'};

    copro_getFwName(buffer);
    printf("Firmware on M4 : %s\n", buffer);

    if(memcmp(firmwareName, buffer, strlen(firmwareName))!=0){
        printf("Need to change firmware\n");
        if( copro_isFwRunning() ){
            printf("Shutting down M4...\n");
            copro_stopFw();
        }else{
            printf("M4 is already down\n");
        }
        printf("Loading firmware...\n");
        copro_setFwName(firmwareName);
        printf("Starting M4...\n");
        copro_startFw();
        printf("Firmware is changed !\n");
        sleep(1); // let time to firmware to start
    }else{
        printf("No need to change firmware\n");
        if( !copro_isFwRunning() ){
            printf("Starting M4...\n");
            copro_startFw();   
            sleep(1); // let time to firmware to start     
        }
    }
}