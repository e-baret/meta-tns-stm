#include <errno.h> 
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h> // write(), read(), close()

struct test{
    uint16_t id;
    uint16_t size;
    uint16_t bytesread;
    uint32_t readtime;
    uint32_t writetime;
    uint32_t globaltime;
    uint8_t state;
};

int echo(int *serial_port, int attemps, int sec);
void init_buffer(char *buffer, uint16_t buffer_size);
void make_test(int *tty_data,int *tty_ack,struct test *test_list, uint16_t nb_tests, uint16_t block_size, int debuglvl);
void print_result(struct test *test_list, int nb_tests, int block_size);
void init_save_file(char *filename);
void save_result(struct test *test_list,char *filename, int nb_tests, int block_size);
void save_result_exceptNULL(struct test *test_list,char *filename, int nb_tests, int block_size);
long timespec_diff_ns (struct timespec t1, struct timespec t2);
