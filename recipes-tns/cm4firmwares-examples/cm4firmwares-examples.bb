SUMMARY = "ELF examples of CM4"
DESCRIPTION = "It installs elf files and scripts to launch examples of CM4"

LICENSE= "CLOSED"

FILES:${PN} += " /lib \
                 /home/root \
                "

SRC_URI += "file://firmwares/ \
            file://scripts/ \
            "
                     
INHIBIT_SYSROOT_STRIP = "1"

do_install () {

    install -d ${D}/lib/firmware
    for file in ${WORKDIR}/firmwares/*;do
        install -m 755 "$file" ${D}/lib/firmware/
    done

    install -d ${D}/home/root/cm4demo    
    for file in ${WORKDIR}/scripts/*;do
        install -m 755 "$file" ${D}/home/root/cm4demo/
    done
}

PACKAGE_ARCH = "${MACHINE_ARCH}"