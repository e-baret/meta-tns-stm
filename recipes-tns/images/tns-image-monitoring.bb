
DESCRIPTION =" "

LICENSE = "MIT"

SECTION = "images"

#require recipes-core/images/core-image-minimal.bb
require tns-image-weston.bb

# Acceptance of the EULA to have acces to all stm features
ACCEPT_EULA_stm32mp1 = "1"

# Define specific board reference to use
M4_BOARDS = "STM32MP157F-DK2"

# Minimal features 
IMAGE_INSTALL += " \
    openssh openssh-sftp-server \
    nano nmon htop \
    util-linux procps \
    e2fsprogs-resize2fs \
    "

# Debug tools
IMAGE_INSTALL += " gdb gdbserver"
IMAGE_INSTALL +=  " strace ltrace valgrind"

# Build tools
IMAGE_INSTALL += " packagegroup-core-buildessential cmake"

# Package management 
IMAGE_INSTALL += " dpkg xz zlib tar"
PACKAGE_CLASSES = "package_rpm"
EXTRA_IMAGE_FEATURES += " package-management"

# vscode ssh remote requirements            
IMAGE_INSTALL += " glibc libc6-dev libstdc++ python3 tar bash curl"

# bus CAN
IMAGE_INSTALL += " can-utils iproute2"

# monitoring tools
IMAGE_INSTALL += " nmon devmem2 stress-ng rt-tests hwlatdetect" 

# dut features
IMAGE_INSTALL += " ckermit"

# weston
#CORE_IMAGE_EXTRA_INSTALL += " wayland weston weston-init weston-xwayland" 
#IMAGE_INSTALL += " l3afpad" 

IMAGE_ROOTFS_MAXSIZE = "2097152"

# =========================================================================
# WIC for sdcard raw image
# =========================================================================
#WIC_CREATE_EXTRA_ARGS = "--no-fstab-update"
WKS_IMAGE_FSTYPES += "wic wic.bz2 wic.bmap"
WKS_FILE += "sdcard-stm32mp157f-dk2-trusted.wks.in"

# -------------------------------------------------------------------------- #
# -------------- Remove unused features from machine stm32mp1 -------------- #
# -------------------------------------------------------------------------- #

# Disable vendorfs and userfs
ST_BOOTFS   = "1"
ST_VENDORFS = "0"
ST_USERFS   = "0"

# Remove unused boot scheme
BOOTSCHEME_LABELS:remove += " optee"

# Remove unused boot device
BOOTDEVICE_LABELS:remove = " emmc nand-4-256 nor-sdcard"

# Remove unused devicetree
STM32MP_DT_FILES_DK:remove += " stm32mp157a-dk1 stm32mp157d-dk1 stm32mp157c-dk2"
STM32MP_DT_FILES_ED:remove += " stm32mp157c-ed1 stm32mp157f-ed1"
STM32MP_DT_FILES_EV:remove += " stm32mp157a-ev1 stm32mp157c-ev1 stm32mp157d-ev1 stm32mp157f-ev1"

# Feature for eval board
KERNEL_MODULE_AUTOLOAD:remove += " goodix"

# Remove unused devicetree for Linux A7 examples
LINUX_A7_EXAMPLES_DT:remove += " stm32mp157c-dk2-a7-examples stm32mp157f-dk2-a7-examples stm32mp157c-ev1-a7-examples stm32mp157f-ev1-a7-examples"

# Remove unused devicetree for M4 example
CUBE_M4_EXAMPLES_DT:remove += " stm32mp157c-dk2-m4-examples stm32mp157f-dk2-m4-examples stm32mp157c-ev1-m4-examples stm32mp157f-ev1-m4-examples"
