
DESCRIPTION ="Image with graphical interface weston"

LICENSE = "MIT"

SECTION = "images"

require tns-image-minimal.bb  

# Images features 
DISTRO_FEATURES += " wayland"

# x11 forwarding
IMAGE_INSTALL += " xauth"

# weston
CORE_IMAGE_EXTRA_INSTALL += " wayland weston weston-init weston-xwayland" 

# virtual keyboard 
#IMAGE_INSTALL += " matchbox-keyboard"
#matchbox-keyboard-applet matchbox-keyboard-im matchbox-config-gtk

# others
IMAGE_INSTALL+= " l3afpad" 

# Image size 
IMAGE_ROOTFS_SIZE = "762336"