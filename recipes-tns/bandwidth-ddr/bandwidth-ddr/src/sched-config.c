#include "sched-config.h"

void change_scheduler(int priority, int policy){
    struct sched_param param;    
    sched_getparam(getpid(), &param);
    param.sched_priority = priority;
    sched_setscheduler(getpid(), policy, &param);
}