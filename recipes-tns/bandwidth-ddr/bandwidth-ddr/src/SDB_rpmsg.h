// C library headers
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <signal.h>
// Linux headers
#include <fcntl.h> // Contains file controls like O_RDWR
#include <errno.h> // Error integer and strerror() function
#include <unistd.h> // write(), read(), close()
#include <sys/mman.h> // mmap() munmap() - map and unmap files/devices in memory
#include <sys/eventfd.h> // event notification
#include <sys/ioctl.h> // control device
#include <sys/time.h>
#include <sys/wait.h>

#include <time.h>
#include <sys/poll.h>
#include <error.h>

#include <assert.h>

#include "sched-config.h"

#define DATA_BUF_POOL_SIZE 1024*1024 // 1MB 
#define NB_BUF 2 //max value = 10

// registre event for buffers
typedef struct{
	int bufferId, eventfd;
} rpmsg_sdb_ioctl_set_efd;

// get size of buffers
typedef struct{
	int bufferId;
	uint32_t size;
} rpmsg_sdb_ioctl_get_data_size;

// send size of buffers
typedef struct{
	int bufferId;
	uint32_t size;
}rpmsg_sdb_ioctl_send_data;

#define RPMSG_SDB_IOCTL_SET_EFD _IOW('R', 0x00, struct rpmsg_sdb_ioctl_set_efd *) //ioctl: register event for a buffer
#define RPMSG_SDB_IOCTL_GET_DATA_SIZE _IOWR('R', 0x01, struct rpmsg_sdb_ioctl_get_data_size *) //ioctl: get the size of a buffer
#define RPMSG_SDB_IOCTL_SEND_DATA _IOWR('R', 0x02, struct rpmsg_sdb_ioctl_send_data *)


struct test{
    uint16_t id;
    uint32_t size;
    uint32_t bytesread;
    uint32_t globaltime;
    uint32_t A7readtime;
    uint32_t A7writetime;
    uint32_t M4readwritetime;
    uint8_t state;
};

int8_t SDB_Init(void);
void SDB_DeInit(void);
int8_t SDB_Write(uint8_t bufferId, char* bufferWrite, uint32_t size);
int8_t SDB_Read(uint8_t bufferId, char* bufferRead, uint32_t size, uint16_t timeout_ms);
void SDB_Clear(uint8_t bufferId, uint32_t size);

void SDB_Echo(int SDBread,int SDBwrite, char* data, int datasize, int timeout);

// Write the buffer "bufferWrite" continuously until having written the requested amount of data (block_size)
int8_t SDB_Write_bigdata(uint8_t bufferId, char* bufferWrite, uint32_t buffersize, uint32_t block_size);

// Wait for message in buffer[bufferId], return the size of datas to read
int32_t SDB_WaitToRead(uint8_t bufferId, uint16_t timeout_ms);

void SDB_bandwidth_test(int SDBread,int SDBwrite,struct test *test_list, uint16_t nb_tests, uint32_t block_size, uint8_t debug);

void init_buffer(char *buffer, uint16_t buffer_size);
void print_result(struct test *test_list, int nb_tests, int block_size);
void init_save_file(char *filename);
void save_result(struct test *test_list,char *filename, int nb_tests, int block_size);
uint32_t timespec_diff_ns (struct timespec t1, struct timespec t2);

void SDB_stress_test(int SDBread,int SDBwrite, char *filename,  uint32_t block_size, uint32_t test_duration_sec, uint32_t time_btw2stress_sec, uint32_t stress_duration_sec, uint8_t test_type, uint8_t worker, uint8_t debug);
void start_stress(uint8_t test_type, uint8_t worker, uint32_t timeout );
