#!/bin/sh

#elf=coproc_sync_CM4.elf
elf=fw-rtos_CM4.elf

echo stop > /sys/class/remoteproc/remoteproc0/state

echo $elf > /sys/class/remoteproc/remoteproc0/firmware

echo start > /sys/class/remoteproc/remoteproc0/state

