SUMMARY = ""
DESCRIPTION = ""

LICENSE= "CLOSED"

FILES:${PN} += " /home/root"
FILES:${PN} += " /usr/include"

SRC_URI += "file://bluetooth/src/ \ 
            file://bluetooth/scripts/ \
            file://bluetooth/readme.md \
            file://wifi/ \
            "
                     
INHIBIT_SYSROOT_STRIP = "1"

DESTDIR_BT = "/home/root/dev/bluetooth"
DESTDIR_WF = "/home/root/dev/wifi"

DEPENDS = " bluez5" 

do_install () {
    # BT 
    install -d ${D}${DESTDIR_BT}
    for file in ${WORKDIR}/bluetooth/src/*;do
        install -m 755 "$file" ${D}${DESTDIR_BT} 
    done
    
    for file in ${WORKDIR}/bluetooth/scripts/*;do
        install -m 755 "$file" ${D}${DESTDIR_BT} 
    done

    install -m 755 ${WORKDIR}/bluetooth/readme.md ${D}${DESTDIR_BT} 

    # WIFI
    install -d ${D}${DESTDIR_WF}
    for file in ${WORKDIR}/wifi/*;do
        install -m 755 "$file" ${D}${DESTDIR_WF} 
    done


}

PACKAGE_ARCH = "${MACHINE_ARCH}"