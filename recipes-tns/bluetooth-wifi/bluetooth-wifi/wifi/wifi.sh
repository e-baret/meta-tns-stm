#!/bin/sh

#wpa_passphrase "<SSID> " "<Wifi password>" >> /etc/wpa_supplicant.conf
ifconfig wlan0 up
echo "wlan0 : Up"
ifconfig wlan0 192.168.7.133
echo "wlan0 : IP 192.168.7.133"
wpa_supplicant -Dnl80211 -iwlan0 -c /etc/wpa_supplicant.conf
ifconfig wlan0 down
echo "wlan0 : Down"
#iw dev wlan0 link
