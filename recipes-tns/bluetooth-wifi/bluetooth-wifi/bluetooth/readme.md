# Device config
Up & Down device:
~~~shell
hcitool hci0 up
hcitool hci0 down
~~~

Change device visibility :
~~~shell
#visible
hciconfig hci0 piscan
#invisible
hciconfig hci0 noscan
~~~

Change MAC adress : 
~~~shell
hcitool cmd 0x3F 0x001 <ADR>
#restart BT to apply changes
hcitool hci0 down
hcitool hci0 up
~~~~


Change BT name : 
~~~shell
hciconfig hci0 name <NAME>
#restart BT to apply changes
hcitool hci0 down
hcitool hci0 up
~~~~

# BT

Scan BT :
~~~shell
hcitool scan
~~~


# BLE

~~~shell
#scan BLE devices
hcitool lescan 
#scan available device
hcitool lewladd <MAC ADR>
#add device to white list 
hcitool lecc <MAC ADR>
~~~


# BT manager  
~~~shell
bluetoothctl
~~~


# C files
depends : libbluetooth-dev

compile : 
~~~shell
gcc < prog >.c -lbluetooth < prog >.o
~~~